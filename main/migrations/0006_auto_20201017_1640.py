# Generated by Django 3.1.2 on 2020-10-17 09:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0005_auto_20201017_1639'),
    ]

    operations = [
        migrations.AlterField(
            model_name='course',
            name='termnyear',
            field=models.CharField(default='', max_length=50, verbose_name='Term and Year'),
        ),
    ]
