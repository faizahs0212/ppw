from django import forms
from django.forms import ModelForm
from .models import *

class CourseForm(forms.ModelForm):
    course = forms.CharField(widget = forms.TextInput(attrs={'placeholder':'Add course'}))
    lecturer = forms.CharField(widget = forms.TextInput(attrs={'placeholder':'Add lecturer'}))
    credit = forms.CharField(widget = forms.TextInput(attrs={'placeholder':'Example: 3'}))
    description = forms.CharField(widget = forms.TextInput(attrs={'placeholder':'Add description to this course'}))
    termnyear = forms.CharField(widget = forms.TextInput(attrs={'placeholder':'Example: GASAL 2019/2020'}), label="Term and Year")
    room = forms.CharField(widget = forms.TextInput(attrs={'placeholder':'Add the classroom'}))

    class Meta:
        model = Course
        fields = '__all__'
