from django.db import models

class Course(models.Model):
    course = models.CharField(max_length=100, default="")
    lecturer = models.CharField(max_length=100, default="")
    credit = models.CharField(max_length=10, default="")
    description = models.CharField(max_length=400, default="")
    termnyear = models.CharField(max_length=50, default="", verbose_name="Term and Year")
    room = models.CharField(max_length=20, default="")

    def __str__(self):
        return self.course + self.lecturer + self.credit + self.description + self.termnyear + self.room
