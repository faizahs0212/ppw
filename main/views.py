from django.shortcuts import render, redirect
from .models import *
from .forms import *
from django.contrib.auth.decorators import login_required

def home(request):
    return render(request, 'main/home.html')

def gallery(request):
    return render(request, 'main/gallery.html')

@login_required(login_url='feature_login:login')
def courses(request):
    if request.method == 'POST':
        form = CourseForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect('/courses')
    
    courses = Course.objects.all()
    form = CourseForm()

    context = {'courses':courses, 'form':form}
    return render(request, 'main/courses.html', context)

def updateCourse(request, pk):
    course = Course.objects.get(id=pk)
    form = CourseForm(instance=course)

    if request.method == 'POST':
        form = CourseForm(request.POST, instance=course)
        if form.is_valid():
            form.save()
            return redirect('/courses')

    context = {'form':form}
    return render(request, 'main/update_course.html', context)

def deleteCourse(request, pk):
    item = Course.objects.get(id=pk)

    if request.method == 'POST':
        item.delete()
        return redirect('/courses')

    context = {'item':item}
    return render(request, 'main/delete_course.html', context)

def detailCourse(request, pk):
    item = Course.objects.get(id=pk)
    context = {'item':item}
    return render(request, 'main/detail_course.html', context)
