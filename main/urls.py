from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('gallery/', views.gallery, name='gallery'),
    path('courses/', views.courses, name='courses'),
    path('courses/<str:pk>/', views.detailCourse, name="detail_course"),
    path('update_course/<str:pk>/', views.updateCourse, name="update_course"),
    path('delete_course/<str:pk>/', views.deleteCourse, name="delete_course"),
]
