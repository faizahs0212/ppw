from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from .forms import createUserForm
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout


# Create your views here.
def signupPage(request):
    if request.user.is_authenticated:
        return redirect('main:home')
    else:
        form = createUserForm()
        if request.method == 'POST':
            form = createUserForm(request.POST)
            if form.is_valid():
                form.save()
                user = form.cleaned_data.get('username')
                messages.success(request, "Your account has been successfully created!")

                return redirect('feature_login:login')

        context = {'form':form}
        return render(request, 'feature_login/signup.html', context)

def loginPage(request):
    if request.user.is_authenticated:
        return redirect('main:home')
    else:
        if request.method == 'POST':
            username = request.POST.get('username')
            password = request.POST.get('password')

            user = authenticate(request, username=username, password=password)

            if user is not None:
                login(request, user)
                return redirect('main:home')
            else:
                messages.info(request, 'Username or password is incorrect')

        context = {}
        return render(request, 'feature_login/login.html', context)

def logoutUser(request):
    logout(request)
    return redirect('main:home')
