from django.urls import path

from . import views

app_name = 'feature_login'

urlpatterns = [
    path('', views.signupPage, name='signup'),
    path('login/', views.loginPage, name='login'),
    path('logout/', views.logoutUser, name='logout'),
]
