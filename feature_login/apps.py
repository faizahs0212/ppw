from django.apps import AppConfig


class FeatureLoginConfig(AppConfig):
    name = 'feature_login'
