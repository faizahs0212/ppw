from django.test import TestCase, Client
from django.urls import resolve

from .views import signupPage, loginPage, logoutUser
from .forms import createUserForm

# Create your tests here.
class TestActivities(TestCase):
   
    def test_url_signup(self):
        response = Client().get('/auth/')
        self.assertEquals(200, response.status_code)

    def test_url_login(self):
        response = Client().get('/auth/login/')
        self.assertEquals(200, response.status_code)

    def test_url_logout(self):
        response = Client().get('/auth/logout/')
        self.assertEquals(302, response.status_code)

    def test_view_func_signup(self):
        found = resolve('/auth/')
        self.assertEqual(found.func, signupPage)

    def test_view_func_login(self):
        found = resolve('/auth/login/')
        self.assertEqual(found.func, loginPage)

    def test_view_func_logout(self):
        found = resolve('/auth/logout/')
        self.assertEqual(found.func, logoutUser)

    def test_template_signup(self):
        response = Client().get('/auth/')
        self.assertTemplateUsed(response, 'feature_login/signup.html')

    def test_template_login(self):
        response = Client().get('/auth/login/')
        self.assertTemplateUsed(response, 'feature_login/login.html')

    def test_page_signup(self):
        response = Client().get('/auth/')
        content = response.content.decode('utf8')

        self.assertIn("REGISTER ACCOUNT", content)
        self.assertIn("Already have an account?", content)
        self.assertIn("Login", content)
    
    def test_page_login(self):
        response = Client().get('/auth/login/')
        content = response.content.decode('utf8')

        self.assertIn("LOGIN", content)
        self.assertIn("Don't have an account?", content)
        self.assertIn("Sign Up", content)

    def test_create_user_form_validation_for_blank_items(self):
        form = createUserForm(data={'username': '', 'password1': '', 'password2': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['username'], ["This field is required."])
        self.assertEqual(form.errors['password1'], ["This field is required."])
        self.assertEqual(form.errors['password2'], ["This field is required."])
