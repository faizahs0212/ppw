from django.urls import path

from . import views

app_name = 'feature_about'

urlpatterns = [
    path('', views.about, name='about'),
]
