from django.test import TestCase, Client
from django.urls import resolve

from .views import about

# Create your tests here.
class TestActivities(TestCase):
    
    def test_url_feature_about(self):
        response = Client().get('/about/')
        self.assertEquals(200, response.status_code)

    def test_template_formulir(self):
        response = Client().get('/about/')
        self.assertTemplateUsed(response, 'feature_about/about.html')

    def test_view_func_create_todo(self):
        found = resolve('/about/')
        self.assertEqual(found.func, about)
