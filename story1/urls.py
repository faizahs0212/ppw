from django.urls import path

from . import views

app_name = 'story1'

urlpatterns = [
    path('', views.story1, name='webStory1'),
]
