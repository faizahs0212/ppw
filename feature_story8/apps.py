from django.apps import AppConfig


class FeatureStory8Config(AppConfig):
    name = 'feature_story8'
