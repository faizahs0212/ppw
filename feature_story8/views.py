from django.shortcuts import render
from django.http import JsonResponse
import requests
import json

# Create your views here.
def story8_func(request):
    return render(request, 'feature_story8/search-book.html')

def book_list_func(request):
    argument = request.GET['q']
    url_destination = 'https://www.googleapis.com/books/v1/volumes?q=' + argument
    req = requests.get(url_destination)

    data = json.loads(req.content)
    return JsonResponse(data, safe=False)
