from django.urls import path

from . import views

app_name = 'story8'

urlpatterns = [
    path('', views.story8_func, name='story8'),
    path('book-list/', views.book_list_func, name='book-list'),
]
