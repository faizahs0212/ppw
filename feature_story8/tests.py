from django.test import TestCase, Client
from django.urls import resolve
import json

from .views import story8_func, book_list_func
# Create your tests here.


# Create your tests here.
class TestActivities(TestCase):
    
    def test_url_feature_story8(self):
        response = Client().get('/story8/')
        self.assertEquals(200, response.status_code)       

    def test_template_search(self):
        response = Client().get('/story8/')
        self.assertTemplateUsed(response, 'feature_story8/search-book.html')

    def test_view_func_story8(self):
        found = resolve('/story8/')
        self.assertEqual(found.func, story8_func)

    def test_page_story8(self):
        response = Client().get('/story8/')
        content = response.content.decode('utf8')

        self.assertIn("What book are you looking for?", content)
        self.assertIn("Book List", content)
        self.assertIn('<input id="keyword" placeholder="Enter a book title" size="50%" style="margin: 2%;">', content)
