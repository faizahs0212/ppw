from django.urls import path

from . import views

app_name = 'activities'

urlpatterns = [
    path('', views.my_activity, name='template_activity'),
]
