from django.shortcuts import render, redirect
from .models import *
from .forms import *

# Create your views here.
def my_activity(request):
    if request.method == 'POST':
        act_form = ActivityForm(request.POST)
        par_form = ParticipantForm(request.POST)
        if act_form.is_valid() and par_form.is_valid():
            act_form.save()
            par_form.save()
        return redirect('/my_activity')
    
    activities = MyActivity.objects.all()
    participants = Participant.objects.all()
    activity_form = ActivityForm()
    participant_form = ParticipantForm()

    context = {'activity':activities, 'activity_form':activity_form, 'participant':participants, 'participant_form':participant_form}
    return render(request, 'activities/my_activity.html', context)
