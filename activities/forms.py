from django import forms
from django.forms import ModelForm
from .models import *

class ActivityForm(forms.ModelForm):
    activity_name = forms.CharField(widget = forms.TextInput(attrs={'placeholder':'Add Activity'}), label="Activity Name")

    class Meta:
        model = MyActivity
        fields = '__all__'

class ParticipantForm(forms.ModelForm):
    participant_name = forms.CharField(widget = forms.TextInput(attrs={'placeholder':'Add Participant'}), label="Participant Name")

    class Meta:
        model = Participant
        fields = '__all__'
