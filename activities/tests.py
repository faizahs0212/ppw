from django.test import TestCase, Client
from .models import *
from .forms import *

class TestActivities(TestCase):

    def test_url_activities(self):
        response = Client().get('/my_activity/')
        self.assertEquals(200, response.status_code)

    def test_template_activity(self):
        response = Client().get('/my_activity/')
        self.assertTemplateUsed(response, 'activities/my_activity.html')

    def test_view_dalam_activity(self):
        response = Client().get('/my_activity/')
        isi_html_kembalian = response.content.decode('utf8')

    def test_model_create_new_activity(self):
        new_activity = MyActivity.objects.create(activity_name='Mengerjakan Story PPW')

        counting_all_available_todo = MyActivity.objects.all().count()
        self.assertEqual(counting_all_available_todo, 1)


    def test_form_activitiy_input_has_placeholder(self):
        form = ActivityForm()
        self.assertIn('placeholder="Add Activity"', form.as_p())

    def test_form_activitiy_input_has_placeholder(self):
        form = ParticipantForm()
        self.assertIn('placeholder="Add Participant"', form.as_p())

    def test_form_validation_for_blank_items(self):
        form = ActivityForm(data={'activity_name': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['activity_name'],
            ["This field is required."]
        )

    def test_form_validation_for_blank_items(self):
        form = ParticipantForm(data={'participant_name': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['participant_name'],
            ["This field is required."]
        )
