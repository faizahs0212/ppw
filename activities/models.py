from django.db import models

# Create your models here.
class MyActivity(models.Model):
    activity_name = models.CharField(max_length=50)

    def __str__(self):
        return self.activity_name

class Participant(models.Model):
    participant_name = models.CharField(max_length=50)
    activity_name = models.ForeignKey(MyActivity, on_delete=models.CASCADE)

    def __str__(self):
        return self.participant_name
